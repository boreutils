/*
 * iota - Generate lists of numbers.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

char sep = ' ';
int size, start, step = 1;

void die(char *name) {
  fprintf(stderr, "usage: %s [-l] size [start] [step]\n", name);
  exit(1);
}

int main(int argc, char *argv[]) {
  int n;
  char c;

  for (n = 1; (c = getopt(argc, argv, "+l")) >= 0; n++) {
    if (c == 'l')
      sep = '\n';
    else if(!isdigit(optopt))
      die(argv[0]);
    else
      break;
  }

  if (n < argc)
    size = atoi(argv[n++]);
  if (n < argc)
    start = atoi(argv[n++]);
  if (n < argc)
    step = atoi(argv[n++]);
  if (n < argc)
    die(argv[0]);

  for (n = 0; n < size; n++, start += step)
    printf("%d%c", start, sep);
  if (size > 0 && sep == ' ')
    putchar('\n');

  return 0;
}
