/**
 * pty - Run command in a new pseudo-tty.
 */

#include <err.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef __linux__
# include <pty.h>
#else
# include <util.h>
#endif

int main(int argc, char **argv) {
	int i, o, n, p;

	if ((p = openpty(&i, &o, NULL, NULL, NULL)) < 0)
		err(1, "openpty");
	if ((n = fork()) < 0)
		err(1, "fork");

	if (n > 0) {
		close(i);
		close(o);
		waitpid(n, NULL, 0);
	} else {
		close(i);
		dup2(o, 0);
		dup2(o, 1);
		dup2(o, 2);
		close(o);
		if (execvp(argv[1], argv + 1) < 0)
			err(1, "exec");
	}

	return 0;
}
