/*
 * tmp - Pass stdin as an argument to a command.
 *
 * For badly-behaved programs that read from a named file rather than
 * standard input.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
  int n, fd;
  char *args[argc];
  char buf[BUFSIZ];
  char tmp[] = "/tmp/tmp.XXXXXX";

  if (argc < 2)
    return 0;

  for (n = 0; n < argc - 1; n++)
    args[n] = argv[n + 1];

  args[n++] = tmp;
  args[n++] = NULL;

  fd = mkstemp(tmp);
  while ((n = read(0, buf, sizeof(buf))))
    write(fd, buf, n);
  close(fd);

  if (fork() == 0) {
    execvp(argv[1], args);
    write(2, "tmp: ", 5);
    perror(argv[1]);
    return errno;
  } else {
    wait(&n);
    remove(tmp);
    return WEXITSTATUS(n);
  }
}
