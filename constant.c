/*
 * constant - Generate C string literal declarations.
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  int i, n, x;
  int status;
  char b[BUFSIZ];

  x = 0;
  status = 0;
  printf("char t[] = {");

  while (1) {
    n = fread(b, sizeof(char), BUFSIZ, stdin);

    if (ferror(stdin)) {
      perror("read");
      status = 1;
    }

    if (n == 0) {
      printf("\n");
      break;
    }

    for (i = 0; i < n; i++) {
      x += printf(x ? " " : "\n  ");
      x += printf("0x%02x,", b[i] & 0xff);
      if (x >= 68) x = 0;
    }
  }

  printf("};\n");

  return status;
}
