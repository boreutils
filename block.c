/*
 * block - Block until a specified time.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

int main(int argc, char *argv[]) {
  time_t when;
  struct timeval t = {
    .tv_sec = 0,
    .tv_usec = 0
  };

  if (argc != 2) {
    fprintf(stderr, "usage: %s timestamp\n", argv[0]);
    return 1;
  }

  if ((when = atoi(argv[1])) < time(0)) {
    return 1;
  }

  t.tv_sec = when - time(0);
  return select(0, NULL, NULL, NULL, &t);
}
