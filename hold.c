/*
 * hold - Accumulate all input before writing it to stdout.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  char *b;
  int n, size, offset;

  setsid();

  offset = 0;
  size = BUFSIZ;

  if ((b = malloc(size)) == NULL) {
    perror("malloc");
    return errno;
  }

  while ((n = read(0, b + offset, BUFSIZ)) > 0) {
    if (size < (offset += n) + BUFSIZ) {
      if ((b = realloc(b, (size *= 2))) == NULL) {
        perror("realloc");
        return errno;
      }
    }
  }

  if (n < 0) {
    perror("read");
    return errno;
  }

  if (write(1, b, offset) < 0) {
    perror("write");
    return errno;
  }

  return 0;
}
