SRC = block.c constant.c errno.c hold.c iota.c pty.c tmp.c
BIN = block constant errno hold iota pty tmp

CFLAGS = -Os -Wall -pedantic -static -lutil

PREFIX = /usr/local

.PHONY: all clean

all: $(BIN)

.c:
	@echo CC $<
	@$(CC) -o $@ $< $(CFLAGS)
	@strip $@

clean:
	@echo RM $(BIN)
	@rm -f $(BIN)

install: $(BIN)
	@echo INSTALL $(PREFIX)/bin
	@mkdir -p $(PREFIX)/bin
	@cp -f $(BIN) $(PREFIX)/bin
	@(cd $(PREFIX)/bin && chmod 755 $(BIN))

uninstall:
	@echo UNINSTALL $(PREFIX)/bin
	@(cd $(PREFIX)/bin && rm -f $(BIN))
