/*
 * errno - Print the string describing an error number.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[]) {
  int n;
  char *s;

  if (argc != 2) {
    fprintf(stderr, "usage: %s number\n", *argv);
    return 1;
  }

  if ((n = atoi(argv[1])) == 0)
    return 0;

  errno = 0;
  s = strerror(n);
  if (errno) {
    perror(*argv);
    return errno;
  }

  puts(s);
  return 0;
}
